if (WIN32)
	# Avoid clashing with windows.h's GDI Polygon function
	add_definitions (-DNOGDI)
endif ()


include_directories(
	${CMAKE_CURRENT_BINARY_DIR}
	${GLEXT_INCLUDE_DIR}
	${OPENGL_INCLUDE_PATH}
)

link_libraries (
	piglitutil_${piglit_target_api}
	${OPENGL_gl_LIBRARY}
)

find_package(Git)

IF( EXISTS ${CMAKE_SOURCE_DIR}/.git AND GIT_FOUND )
	EXECUTE_PROCESS( COMMAND "${GIT_EXECUTABLE}" -C "${CMAKE_SOURCE_DIR}" rev-parse HEAD
                         WORKING_DIRECTORY "${DIR}"
                         RESULT_VARIABLE   GIT_RESULT
                         OUTPUT_VARIABLE   "GIT_HEAD_HASH"
                         ERROR_VARIABLE    GIT_ERROR
                         OUTPUT_STRIP_TRAILING_WHITESPACE )

        IF( NOT ${GIT_RESULT} EQUAL 0 )
            MESSAGE( SEND_ERROR "Command '${GIT_EXECUTABLE} -C ${CMAKE_SOURCE_DIR} rev-parse HEAD' failed with following output:\n${GIT_ERROR}" )
        ENDIF( NOT ${GIT_RESULT} EQUAL 0 )
ENDIF( EXISTS ${CMAKE_SOURCE_DIR}/.git AND GIT_FOUND )

set ("GIT_HEAD_HASH"       "${GIT_HEAD_HASH}"      )

configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/version.h.in
    ${CMAKE_CURRENT_BINARY_DIR}/version.h
    @ONLY
)

piglit_add_executable (llvmpipe llvmpipe.cpp glsl/shader.cpp tests/test.cpp tests/state.cpp utils/piglit-ext.cpp utils/random.cpp utils/glenum.cpp)


# vim: ft=cmake:
